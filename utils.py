from datetime import datetime
from collections import deque

cache_max_size = 10
cache = deque(maxlen=cache_max_size)
cache_ra = deque(maxlen=cache_max_size)


def add_to_cache(item):

    # If cache is full, removes the oldest element
    if len(cache) == cache.maxlen:
        cache.pop()
        cache_ra.pop()

    cache_ra.appendleft(item["ra"])
    cache.appendleft(item)

    return True


def get_from_cache(ra):

    idx_ra = cache_ra.index(ra)
    item = cache[idx_ra]

    # Updating cache after getting the needed element
    cache.remove(item)
    cache_ra.remove(ra)
    cache.appendleft(item)
    cache_ra.appendleft(ra)

    return item


def dates_validate(start, end):

    date_format = '%Y-%m-%d'

    try:
        start = datetime.strptime(start, date_format)
        end = datetime.strptime(end, date_format)
    except ValueError:
        code = 400
        response = {
            "response": {"message": "Incorrect date format, should be YYYY-MM-DD"},
            "status": 400,
            "mimetype": 'application/json'
        }
        return response, code

    if start > end:
        code = 400
        response = {
            "response": {"message": "Start date can't be greater than end date"},
            "status": 400,
            "mimetype": 'application/json'
        }
        return response, code
    else:
        return None, None
