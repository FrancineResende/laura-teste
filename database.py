from pymongo import MongoClient
import os

# Getting environment variables from env_file.env
# To export then, run $ 'export $(cat env_file.env)'

client = MongoClient(os.environ.get('HOST'), int(os.environ.get('PORT')))
db = client[os.environ.get('DB_NAME')]
collection = db[os.environ.get('COLLECTION_NAME')]
