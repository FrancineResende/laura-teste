#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pymongo import MongoClient, ASCENDING, DESCENDING
import pandas as pd
import json
import os

# Getting environment variables from env_file.env
# To export then, run: $ 'export $(cat env_file.env)'

client = MongoClient(os.environ.get('HOST'), int(os.environ.get('PORT')))
db = client[os.environ.get('DB_NAME')]
collection = db[os.environ.get('COLLECTION_NAME')]
# collection.create_index([("modalidade", ASCENDING), ("_id", DESCENDING)])
# collection.create_index([("campus", ASCENDING), ("curso", ASCENDING)])
# collection.create_index([("campus", ASCENDING), ("ra", ASCENDING)])
collection.create_index("ra", unique=True)

file = pd.read_csv('dataset_estudantes.csv', encoding='utf-8')
file = file.drop_duplicates('ra').dropna(subset=['ra'])
# Removing data with duplicated or missing ra values before transform it to json
data = json.loads(file.to_json(orient='records', force_ascii=False))

try:
    collection.insert_many(data, ordered=False)
    if collection.acknowledged is True:
        print("Success on loading data")
except Exception:
    print("Error on loading data")

client.close()
