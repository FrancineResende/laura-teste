# Laura-teste

Uma API utilizando Flask e PyMongo, desenvolvida como forma de teste prático no processo seletivo para vaga de backend developer na empresa Laura. Os endpoints da API trabalham com os dados inseridos no banco através do arquivo dataset_students.csv

## Descrição dos arquivos criados:

#### import_data.py
Script que importa os dados do arquivo _dataset_students.csv_ e faz um pequeno tratamento no conjunto antes de inseri-lo no banco.    

#### app.py  
É o principal arquivo da API. Ele roda a aplicação e controla o encerramento das conexões ao fim de cada requisição.

#### database.py  
É o arquivo que configura a conexão com o banco e cria os objetos utilizados na manipulação da _collection_.

#### utils.py
Contém funções de apoio, como a validação de datas e a lógica interna do cache desenvolvido.

#### endpoints.py  
Contém todas as funções executadas nos endpoints da API


## Requisitos
1. Uma instância do mongodb deve estar rodando localmente, com permissão de livre acesso pelo ip local.
2. Um ambiente virtual deve estar criado e ativo. Se estiver utilizando virtualenv, ativar o ambiente com:

```bash
source <path_to_env>/bin/activate
```

## Configuração e preparação
Após garantir que o ambiente virtual esteja ativo, instale as dependências necessárias.

```bash
pip install -r requirements.txt
```

Agora, é necessário exportar as variáveis de ambiente.

```bash
export $(cat env_file.env)
```

Popule o banco de dados executando o arquivo import_data.py.

```bash
python import_data.py
```

Finalmente, execute a API utilizando o comando do Flask:

```bash
flask run
```

## Utilização
Por padrão, o Flask utiliza o ip local e a porta 5000 para executar a API. Dessa forma, a base da url de todas as requisições é http://localhost:5000.

Existem diferentes maneiras de executar as requisições. Aqui deixarei exemplos de como executá-las utilizando a ferramenta cURL.

###### Endpoint 1 - Listar todos os itens de uma modalidade em um período ordenados por data
```bash
curl -X GET http://localhost:5000/modalidade/<modalidade>/<data_inicio>/<data_fim>
```
Se for inserido um nome de modalidade inexistente, a API responderá com código 404. O mesmo também acontecerá se por algum motivo não for encontrado pelo menos um registro no período informado, mesmo que o nome da modalidade esteja correto.

A API recusará uma data inicial maior que a data final ou qualquer formato diferente de YYYY-MM-DD. Se isso acontecer, ela responde com código 400.

---
###### Endpoint 2 - Listar todos os cursos de um campus
```bash
curl -X GET http://localhost:5000/cursos_por_campus/<campus>
```
Responde com código 404 caso o resultado seja vazio. Isso acontecerá se a entrada for um campus que não existe.

---
###### Endpoint 3 - Descobrir número total de alunos num campus em um dado período
```bash
curl -X GET http://localhost:5000/alunos_campus_periodo/<campus>/<data_inicio>/<data_fim>
```
Este endpoint retorna código 200 mesmo que o número de alunos encontrados seja 0 (zero). Somente retornará código de erro se algum dos campos de data não passar na validação.

---
###### Endpoint 4 - Cadastrar alunos
```bash
curl -X PUT -H "Content-Type: application/json" -d <input_json> http://localhost:5000/cadastrar_aluno
```
Deve-se utilizar como entrada para este endpoint um objeto JSON com as mesmas chaves da _collection_ criada (com exceção da chave _id). Exemplo:

```python
aluno = {
         "nome": 'João da Silva',
         "idade_ate_31_12_2016": 18,
         "ra": 9999.9,
         "campus": ABC,
         "município": Curitiba,
         "curso": 'CIÊNCIA DA COMPUTAÇÃO',
         "modalidade": PRESENCIAL,
         "nivel_do_curso": BACHARELADO,
         "data_inicio": 2016-03-02
         }
```
O banco recusará uma inserção que contenha um valor de _ra_ já existente.

Este endpoint adiciona o aluno também no cache.

---
###### Endpoint 5 - Buscar aluno
```bash
curl -X GET  http://localhost:5000/buscar_aluno/<ra>
```
Apenas retorna o aluno encontrado ou responde com 404 caso não haja nenhum registro com o ra informado.

Pode ler do cache ou inserir nele.

Obs: Acredito que este endpoint deveria utilizar o método POST, mas devido a problemas técnicos acabei implementando com o GET mesmo.

---
###### Endpoint 6 - Remover aluno do banco
```bash
curl -X DELETE  http://localhost:5000/deleta/<ra>/<campus>
```
Retorna erro se não encontrar um registro que contenha os valores informados. Caso contrário, faz a remoção e retorna sucesso.

---
### Problemas conhecidos
- Implementação do método incorreto no endpoint 5 (utilizado GET ao invés de POST).
- Não consegui corrigir a codificação dos caracteres non-ascii na hora de retornar as respostas.
