from flask import Flask, request
from database import collection
from pymongo import DESCENDING
import utils
mimetype = "application/json"

app = Flask(__name__)


@app.route("/modalidade/<modality>/<start_date>/<end_date>", methods=["GET"])
# Endpoint 1
def lista_itens_modalidade(modality, start_date, end_date):

    # If any date parameter is not valid, the returning code will be 400
    response, code = utils.dates_validate(start_date, end_date)
    if code is not None:
        return response, code

    cursor = collection.find(filter={"modalidade": modality, "data_inicio":
                                     {"$gte": start_date, "$lte": end_date}})\
                       .sort("_id", DESCENDING)

    result = []
    for document in cursor:
        document.pop("_id")
        result.append(document)

    if len(result) > 0:
        code = 200
        response = {
            "response": result,
            "status": 200,
            "mimetype": mimetype
        }
    else:
        code = 404
        response = {
            "response": {"message": "Query doesn't found any result"},
            "status": 404,
            "mimetype": mimetype
        }

    return response, code


@app.route("/cursos_por_campus/<campus>", methods=["GET"])
# Endpoint 2
def lista_cursos(campus):

    cursor = collection.find({"campus": campus})

    result = set()
    for res in cursor:
        result.add(res['curso'])

    if len(result) > 0:
        code = 200
        response = {
            "response": sorted(result),
            "status": 200,
            "mimetype": mimetype
        }
    else:
        code = 404
        response = {
            "response": {"message": "Query doesn't found any result"},
            "status": 404,
            "mimetype": mimetype
        }

    return response, code


@app.route("/alunos_campus_periodo/<campus>/<start_date>/<end_date>", methods=["GET"])
# Endpoint 3
def conta_alunos(campus, start_date, end_date):

    # If any date parameter is not valid, the returning code will be 400
    response, code = utils.dates_validate(start_date, end_date)
    if code == 400:
        return response, code

    cursor = collection.find({"campus": campus,
                              "data_inicio": {"$gte": start_date, "$lte": end_date}})

    result = set()
    for res in cursor:
        result.add(res['ra'])

    response = {
        "response": {"quantidade_alunos": len(result)},
        "status": 200,
        "mimetype": mimetype
    }

    return response


@app.route("/cadastrar_aluno", methods=["PUT"])
# Endpoint 4
def cadastra_aluno():

    input = request.get_json()
    # It is needed to validate the input before trying to insert, but I didn't do this

    try:
        collection.insert_one(input)
        utils.add_to_cache(input)
        code = 200
        response = {
            "response": "Success",
            "status": 200,
            "mimetype": mimetype
        }
    except Exception:
        code = 400
        response = {
            "response": "Error",
            "status": 400,
            "mimetype": mimetype
        }

    return response, code


@app.route("/buscar_aluno/<float:ra>", methods=["GET"])
# Endpoint 5 - # This endpoint should use the POST method instead of GET
def busca_aluno(ra):

    if ra in utils.cache_ra:
        cursor = utils.get_from_cache(ra)
    else:
        cursor = collection.find_one({"ra": ra})

    if cursor is not None:
        if '_id' in cursor.keys():
            del cursor["_id"]
        if ra not in utils.cache_ra:
            utils.add_to_cache(cursor)
        code = 200
        response = {
            "response": cursor,
            "status": 200,
            "mimetype": mimetype
        }
    else:
        code = 404
        response = {
            "response": {"message": "Query doesn't found any result"},
            "status": 404,
            "mimetype": mimetype
        }

    return response, code


@app.route("/deleta/<float:ra>/<campus>", methods=["DELETE"])
# Endpoint 6
def delete(ra, campus):
    result = collection.delete_one({"ra": ra, "campus": campus})
    if result.deleted_count > 0:
        code = 200
        response = {
            "response": "Success",
            "status": 200,
            "mimetype": mimetype
        }
    else:
        code = 400
        response = {
            "response": "Error",
            "status": 400,
            "mimetype": mimetype
        }

    return response, code
