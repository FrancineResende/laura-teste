#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from endpoints import app
from database import client


@app.teardown_request
def close_session(exception=None):
    # Closes session after every request
    print('Closing mongo session')
    client.close()


def main():
    app.run(debug=True)


if __name__ == "__app__":
    main()
